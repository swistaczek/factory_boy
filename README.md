# factory_boy
[![wercker status](https://app.wercker.com/status/e70c2eadffd4a6c8dde8bb0371ee97af/s/master "wercker status")](https://app.wercker.com/project/byKey/e70c2eadffd4a6c8dde8bb0371ee97af)

Fixtures replacement with easy definition syntax and support for multiple factories for the same class.

## Usage
Add the following line to Gemfile:

```ruby
gem 'factory_boy'
```

#### Short usage example
```ruby
require 'factory_boy'

# Let's create User class in object namespace
class User
  attr_accessor :name, :admin
end

# Define factory for default user
FactoryBoy.define_factory(:user) do
  name 'foobar'
end

# Define factory for admin user
FactoryBoy.define_factory(:admin, class_name: User) do
  name 'powerhorse'
  admin true
end

# Lets build our users
user = FactoryBoy.build(:user)    # => #<User:0x007fccbb0486e0 @name="foobar">
admin = FactoryBoy.build(:admin)  # => #<User:0x007fccbb093898 @admin=true, @name="powerhorse">
```

## Contributing to factory_boy
* Check out the latest master to make sure the feature hasn't been implemented or the bug hasn't been fixed yet.
* Check out the issue tracker to make sure someone already hasn't requested it and/or contributed it.
* Fork the project.
* Start a feature/bugfix branch.
* Commit and push until you are happy with your contribution.
* Make sure to add tests for it. This is important so I don't break it in a future version unintentionally.
* Please try not to mess with the Rakefile, version, or history. If you want to have your own version, or is otherwise necessary, that is fine, but please isolate to its own commit so I can cherry-pick around it.

## Copyright
Copyright (c) 2016 Ernest Bursa. See ```LICENSE.txt``` for
further details.
