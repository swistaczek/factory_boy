# encoding: utf-8
require_relative 'factory_boy/exceptions'
require_relative 'factory_boy/factory'

# Main module
module FactoryBoy
  @repository = {}

  class << self
    attr_reader :repository

    def define_factory(alias_key, opts = {}, &block)
      klass = obtain_class(alias_key, opts)

      factory = Factory.new(klass)
      factory.instance_eval(&block)

      @repository[alias_key] = factory
    end

    def build(klass, custom_attributes = {})
      if @repository.has_key?(klass)
        factory   = @repository[klass]
        klass_def = factory.klass
        instance  = klass_def.new

        (factory.attributes || {}).merge(custom_attributes).each do |key,value|
          instance.public_send([key, '='].join, value)
        end

        instance
      else
        raise Exceptions::FactoryNotDefined, "Factory for #{klass} was not defined"
      end
    end

    def reset_definitions!
      @repository = {}
    end

    private

    # Checks if user defined aliased factory
    def obtain_class(klass_or_alias, opts = {})
      transform_to_class(opts[:class_name] || klass_or_alias)
    end

    # Return class definition object
    def transform_to_class(klass)
      case [klass.class]
      when [Symbol], [String]
          Object.const_get(klass.to_s.split('_').collect(&:capitalize).join)
      else
          klass
      end
    end

  end


end
