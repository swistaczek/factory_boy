# encoding: utf-8
module FactoryBoy
  # Groups lib exceptions
  module Exceptions
    # Raised when there is no defined factory for given key
    class FactoryNotDefined < StandardError; end
  end
end
