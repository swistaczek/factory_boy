# encoding: utf-8
module FactoryBoy
  module Test
    module Fixtures
      class << self
        def define_user_class!
          Object.const_set(:User, Class.new do
            attr_accessor :name, :admin
          end)
        end

        def define_class_without_fixture!
          Object.const_set(:ClassWithoutFixture, Class.new do
            attr_accessor :foo
          end)
        end

        def clear_objectspace!
          Object.send(:remove_const, :User) if defined?(User)
          Object.send(:remove_const, :ClassWithoutFixture) if defined?(ClassWithoutFixture)
        end
      end
    end
  end
end
