# encoding: utf-8
require 'helper'

class FactoryBoySpec < Minitest::Test
  def setup
    FactoryBoy::Test::Fixtures.clear_objectspace!
    FactoryBoy.reset_definitions!
  end

  def test_build_for_not_existing_class
    # prepare
    FactoryBoy::Test::Fixtures.define_class_without_fixture!

    # assert
    assert_raises FactoryBoy::Exceptions::FactoryNotDefined do
      FactoryBoy.build(:class_without_fixture)
    end
  end

  def test_build_user_with_default_attributes
    # prepare
    FactoryBoy::Test::Fixtures.define_user_class!
    FactoryBoy.define_factory(:user) { name 'foobar' }

    desired_instance      = User.new
    desired_instance.name = 'foobar'
    factoried_instance = FactoryBoy.build(:user)

    # assert
    assert_equal factoried_instance.name, desired_instance.name
    assert_equal factoried_instance.class, User
  end

  def test_build_user_with_overridden_attributes
    # prepare
    FactoryBoy::Test::Fixtures.define_user_class!
    FactoryBoy.define_factory(:user) { name 'notJanuszek' }
    new_name = 'Januszek'

    desired_instance      = User.new
    desired_instance.name = new_name
    factoried_instance = FactoryBoy.build(:user, name: new_name)

    assert_equal factoried_instance.name, desired_instance.name
    assert_equal factoried_instance.class, User
  end
end
